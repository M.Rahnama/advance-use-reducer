import { useQuizContext } from "../contexts/QuizContext";

function StartScreen() {
  const props = useQuizContext();
  return (
    <div className="start">
      <h2>Welcome to The React Quiz!</h2>
      <h3>{props.numQuestion} questions to test your React mastery</h3>
      <button
        className="btn btn-ui"
        onClick={() => props.dispatch({ type: "ACTIVATE" })}
      >
        Lets start
      </button>
    </div>
  );
}

export default StartScreen;
