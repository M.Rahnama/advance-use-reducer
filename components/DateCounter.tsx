"use client";
import { useState, useReducer } from "react";

const initialState = { count: 0, step: 1 };
// نوع حالت (state) شمارنده
type CounterState = {
  count: number;
  step: number;
};

// نوع عملیات (action) شمارنده
type CounterAction =
  | { type: "increment" }
  | { type: "decrement" }
  | {
      payload: number;
      type: "setStep";
    }
  | { type: "reset" }
  | {
      type: "setCounter";
    };

const counterReducer = (
  state: CounterState,
  action: CounterAction
): CounterState => {
  switch (action.type) {
    case "increment":
      return { ...state, count: state.count + state.step };
    case "decrement":
      return { ...state, count: state.count - state.step };
    case "setStep":
      return { ...state, step: action.payload };
    case "setCounter":
      return { ...state, count: state.count };
    case "reset":
      return initialState;
    default:
      throw new Error("Invalid action");
  }
};
function DateCounter() {
  //   const [count, setCount] = useState(0);
  const [count, dispatch] = useReducer(counterReducer, initialState);
  const [step, setStep] = useState(1);

  // This mutates the date object.
  const date = new Date("june 21 2027");
  date.setDate(date.getDate() + count.count);

  const dec = function () {
    dispatch({ type: "decrement" });
  };

  const inc = function () {
    dispatch({ type: "increment" });
  };

  const defineCount = function (e: { target: { value: any } }) {
    // setCount(Number(e.target.value));
    dispatch({ type: "setCounter" });
  };

  const defineStep = function (e: { target: { value: any } }) {
    // setStep(Number(e.target.value));
    dispatch({ type: "setStep", payload: Number(e.target.value) });
  };

  const reset = function () {
    //   setCount(0);
    dispatch({ type: "reset" });
    setStep(1);
  };

  return (
    <div className="counter">
      <div>
        <input
          type="range"
          min="0"
          max="10"
          value={count.step}
          onChange={defineStep}
        />
        <span>{count.step}</span>
      </div>

      <div>
        <button onClick={dec}>-</button>
        <input value={count.count} onChange={defineCount} />
        <button onClick={inc}>+</button>
      </div>

      <p>{date.toDateString()}</p>

      <div>
        <button onClick={reset}>Reset</button>
      </div>
    </div>
  );
}
export default DateCounter;
