function Header() {
  return (
    <header className="app-header">
      <img src="next.svg" alt="React logo" />
      <h5>The React Quiz</h5>
    </header>
  );
}

export default Header;
