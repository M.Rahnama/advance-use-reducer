import React, { useRef, useImperativeHandle, forwardRef } from "react";

export interface CustomInputRef {
  focusInput: () => void;
}

interface CustomInputProps {
  ref: any;
  // props کامپوننت ورودی سفارشی
}

const CustomInput: React.FC<CustomInputProps> = forwardRef<
  CustomInputRef,
  CustomInputProps
>((props, ref) => {
  const inputRef = useRef<HTMLInputElement>(null);

  useImperativeHandle(ref, () => ({
    focusInput: () => {
      if (inputRef.current) {
        inputRef.current.focus();
      }
    },
  }));

  return <input type="text" ref={inputRef} />;
});

CustomInput.displayName = "CustomInput";
export default CustomInput;
