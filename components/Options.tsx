import React from "react";
import "../src/app/globals.css";
function Options({
  data,
  dispatch,
  answer = "",
}: {
  data: any;
  dispatch: any;
  answer: any;
}) {
  const hasAnswer = answer !== null;
  return (
    <div className="flex flex-col gap-4">
      {data.options.map((option: any, index: any) => (
        <button
          key={option}
          disabled={hasAnswer}
          className={`btn btn-option  ${index === answer ? "answer" : ""} ${
            hasAnswer && index === data.correctOption ? "correct" : "wrong"
          }`}
          onClick={() => dispatch({ type: "NEWANSWER", payload: index })}
        >
          {option}
        </button>
      ))}
    </div>
  );
}

export default Options;
