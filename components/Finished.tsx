import React from "react";
import { useQuizContext } from "../contexts/QuizContext";

const Finished = () => {
  const props = useQuizContext();

  const percentage = (props.points / props.allPoints) * 100;
  return (
    <>
      <p className="result w-full">
        You scored <strong>{props.points}</strong> of {props.allPoints} (
        {Math.ceil(percentage)}%)
      </p>
      <span className="highScore">Your highScore {props.highScore}</span>
      <button
        className="btn btn-ui"
        onClick={() => props.dispatch({ type: "RESET" })}
      >
        Reset
      </button>
    </>
  );
};

export default Finished;
