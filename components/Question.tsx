import React, { useEffect } from "react";
import Options from "./Options";
import { useQuizContext } from "../contexts/QuizContext";

const Question = () => {
  const { index, secondRemaining, numQuestion, dispatch, data, answer } =
    useQuizContext();

  const min = Math.floor(secondRemaining / 60);
  const sec = secondRemaining % 60;
  const nextQuestionHandler = () => {
    if (index < numQuestion - 1) {
      dispatch({ type: "NEXTQUESTION" });
    } else {
      dispatch({ type: "FINISHED" });
    }
  };
  useEffect(() => {
    const timer = setInterval(() => {
      dispatch({ type: "TIMER" });
    }, 1000);
    return () => clearInterval(timer);
  }, []);

  return (
    <div>
      <>
        <h4>{data.question}</h4>
        <Options data={data} dispatch={dispatch} answer={answer} />
        <span className="timer">
          {min < 10 && "0"}
          {min}:{sec < 10 && "0"}
          {sec}
        </span>
        {answer !== null && (
          <>
            <button
              className="btn btn-ui mt-4"
              onClick={() => nextQuestionHandler()}
            >
              {index < numQuestion - 1 ? "Next" : "Finish"}
            </button>
          </>
        )}
      </>
    </div>
  );
};

export default Question;
