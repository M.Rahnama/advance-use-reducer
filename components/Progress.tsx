import React from "react";
import { useQuizContext } from "../contexts/QuizContext";

const Progress = () => {
  const props = useQuizContext();
  return (
    <header className="progress flex justify-between w-full">
      <progress
        max={props.numQuestion}
        value={props.index + Number(props.answer !== null)}
      />
      <p>
        Question <strong>{props.index + 1}</strong> / {props.numQuestion}
      </p>
      <p>
        <strong>{props.points}</strong>/{props.allPoints} Points
      </p>
    </header>
  );
};

export default Progress;
