"use client";

import { useReducer } from "react";

// نوع حالت (state) شمارنده
type CounterState = {
  count: number;
};

// نوع عملیات (action) شمارنده
type CounterAction =
  | { type: "increment" }
  | { type: "decrement" }
  | { type: "reset" };

// تابع کاهش اعداد به حالت (state) جدید
const counterReducer = (
  state: CounterState,
  action: CounterAction
): CounterState => {
  switch (action.type) {
    case "increment":
      return { count: state.count + 1 };
    case "decrement":
      return { count: state.count - 1 };
    case "reset":
      return { count: 0 };
    default:
      throw new Error("Invalid action");
  }
};

const Counter = () => {
  // استفاده از useReducer برای مدیریت حالت شمارنده
  const [state, dispatch] = useReducer(counterReducer, { count: 0 });

  return (
    <div>
      <p>Count: {state.count}</p>
      <button onClick={() => dispatch({ type: "increment" })}>Increment</button>
      <button onClick={() => dispatch({ type: "decrement" })}>Decrement</button>
      <button onClick={() => dispatch({ type: "reset" })}>Reset</button>
    </div>
  );
};

export default Counter;
