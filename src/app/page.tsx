"use client";
import { useRef, useEffect, useReducer, useContext } from "react";
import CustomInput, { CustomInputRef } from "../../components/CustomInput";
import Header from "../../components/Header";
import Main from "../../components/Main";
import axios from "axios";
import Loader from "../../components/Loader";
import Error from "../../components/Error";
import StartScreen from "../../components/StartScreen";
import Question from "../../components/Question";
import Progress from "../../components/Progress";
import Finished from "../../components/Finished";
import { QuizProvider, useQuizContext } from "../../contexts/QuizContext";

export default function Home() {
  // const customInputRef = useRef<CustomInputRef>(null);
  // const handleClick = () => {
  //   if (customInputRef.current) {
  //     customInputRef.current.focusInput();
  //   }
  // };

  return (
    <div>
      {/* <div className="mt-8">
        <CustomInput ref={customInputRef} />
        </main>
        <button onClick={handleClick}>فوکوس روی ورودی</button>
      </div>  */}
      <Header />
      <Main>
        <QuizProvider />
      </Main>
    </div>
  );
}
