import axios from "axios";
import React, {
  Dispatch,
  ReactNode,
  createContext,
  useContext,
  useEffect,
  useReducer,
} from "react";
import Loader from "../components/Loader";
import Error from "../components/Error";
import StartScreen from "../components/StartScreen";
import Progress from "../components/Progress";
import Question from "../components/Question";
import Finished from "../components/Finished";

const initialState = {
  index: 0,
  question: [],
  answer: null,
  points: 0,
  status: "loading",
  highScore: 0,
  secondRemaining: 40,
};
const initialValue = {
  index: 0,
  answer: null,
  points: 0,
  status: "loading",
  highScore: 0,
  secondRemaining: 40,
  allPoints: 0,
  numQuestion: 0,
  dispatch: (() => undefined) as Dispatch<any>,
  data: {
    question: "",
    options: [],
    correctOption: 0,
    points: 0,
  },
};

const ANSWER_PER_SECOND = 30;

const reducer = (state: any, action: { type: any; payload?: any }) => {
  switch (action.type) {
    case "GETDATA":
      return { ...state, question: action.payload, status: "isReady" };
    case "FAILEDGETDATA":
      return { ...state, status: "error" };
    case "ACTIVATE":
      return {
        ...state,
        secondRemaining: state.question.length * ANSWER_PER_SECOND,
        status: "isActivate",
      };
    case "NEWANSWER":
      const question = state.question.at(state.index);
      return {
        ...state,
        answer: action.payload,
        points:
          action.payload === question.correctOption
            ? state.points + question.points
            : state.points,
      };
    case "NEXTQUESTION":
      return {
        ...state,
        index: state.index + 1,
        answer: null,
      };
    case "FINISHED":
      return {
        ...state,
        status: "finished",
        highScore:
          state.points > state.highScore ? state.points : state.highScore,
      };
    case "TIMER":
      return {
        ...state,
        status: state.secondRemaining === 0 ? "finished" : state.status,
        secondRemaining: state.secondRemaining - 1,
      };
    case "RESET":
      return {
        ...state,
        index: 0,
        points: 0,
        answer: null,
        secondRemaining: state.question * ANSWER_PER_SECOND,
        status: "reset",
      };

    default:
      break;
  }
};
const quizContext = createContext(initialValue);
const QuizProvider = () => {
  const [
    { question, status, index, answer, points, highScore, secondRemaining },
    dispatch,
  ] = useReducer(reducer, initialState);

  useEffect(() => {
    axios
      .get("http://localhost:8000/questions")
      .then(({ data }) => dispatch({ type: "GETDATA", payload: data }))
      .catch((err) =>
        dispatch({
          type: "FAILEDGETDATA",
        })
      );
  }, []);

  const allPoints = question.reduce(
    (prev: number, cur: { points: number }) => prev + cur.points,
    0
  );
  return (
    <quizContext.Provider
      value={{
        index,
        numQuestion: question.length,
        points,
        allPoints,
        answer,
        status,
        dispatch,
        secondRemaining,
        data: question[index],
        highScore,
      }}
    >
      {status === "loading" && <Loader />}
      {status === "error" && <Error />}
      {status === "isReady" && <StartScreen />}
      {(status === "isActivate" || status === "reset") && (
        <>
          <Progress />
          <Question />
        </>
      )}
      {status === "finished" && <Finished />}
    </quizContext.Provider>
  );
};

function useQuizContext() {
  const context = useContext(quizContext);
  return context;
}

export { QuizProvider, useQuizContext };
